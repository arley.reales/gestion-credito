package co.com.reales;

import co.com.reales.model.cliente.gateway.ClienteRepository;
import co.com.reales.usecase.cliente.CrearClienteUseCase;
import org.reactivecommons.utils.ObjectMapper;
import org.reactivecommons.utils.ObjectMapperImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesConfig {
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapperImp();
    }

    @Bean
    public CrearClienteUseCase crearClienteUseCase(ClienteRepository clienteRepository) {
        return new CrearClienteUseCase(clienteRepository);
    }
}
