package co.com.reales.defaults;

import co.com.reales.model.cliente.Cliente;
import co.com.reales.model.cliente.gateway.ClienteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

@Slf4j
@Configuration
public class DefaultBeansConfig {
    @Bean
    @ConditionalOnMissingBean
    public ClienteRepository clienteRepository() {
        return clienteRepository;
    }

    private final ClienteRepository clienteRepository = new ClienteRepository() {
        @Override
        public Mono<Cliente> crearCliente(Cliente cliente) {
            return null;
        }

        @Override
        public Mono<Boolean> existeCliente(String cedulaCliente) {
            return null;
        }
    };
}
