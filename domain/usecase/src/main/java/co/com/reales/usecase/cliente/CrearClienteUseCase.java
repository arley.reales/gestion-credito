package co.com.reales.usecase.cliente;

import co.com.reales.model.cliente.Cliente;
import co.com.reales.model.cliente.gateway.ClienteRepository;
import co.com.reales.model.common.ex.ClienteException;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class CrearClienteUseCase {
    private final ClienteRepository clienteRepository;

    public Mono<Cliente> crearCliente(Cliente cliente) {
        return clienteRepository.existeCliente(cliente.getCedula())
                .flatMap(esxiste ->
                        Boolean.TRUE.equals(esxiste)
                                ? Mono.error(ClienteException.Type.EXISTE_CLIENTE.build())
                                : clienteRepository.crearCliente(cliente));
    }
}
