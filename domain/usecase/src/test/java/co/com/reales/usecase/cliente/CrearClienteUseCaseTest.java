package co.com.reales.usecase.cliente;

import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import co.com.reales.model.cliente.gateway.ClienteRepository;
import co.com.reales.model.common.ex.ClienteException;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
public class CrearClienteUseCaseTest {
    @Mock
    private ClienteRepository clienteRepository;

    @InjectMocks
    private CrearClienteUseCase crearClienteUseCase;

    private final static String CEDULA_CLIENTE = "987654321";
    private final static String ID_COBRO = "idCobro";

    @Test
    @DisplayName("test crear cliente que retorna un error ya que existe un cliente con la misma cedula")
    public void testCrearClienteErrorExisteCliente() {
        //Datos Entrada
        Cliente cliente = getCliente()
                .cedula(CEDULA_CLIENTE)
                .idCobro(ID_COBRO)
                .build();

        //Datos Salida

        Mockito.when(clienteRepository.existeCliente(CEDULA_CLIENTE))
                .thenReturn(Mono.just(Boolean.TRUE));

        StepVerifier.create(crearClienteUseCase.crearCliente(cliente))
                .expectErrorMessage(ClienteException.Type.EXISTE_CLIENTE.getMessageFrontEnd())
                .verify();
    }

    @Test
    @DisplayName("test crear cliente se crea exitosamente el cliente")
    public void testCrearClienteExitoso() {
        //Datos Entrada
        Cliente cliente = getCliente()
                .cedula(CEDULA_CLIENTE)
                .idCobro(ID_COBRO)
                .build();

        //Datos Salida

        Mockito.when(clienteRepository.existeCliente(CEDULA_CLIENTE))
                .thenReturn(Mono.just(Boolean.FALSE));

        Mockito.when(clienteRepository.crearCliente(cliente))
                .thenReturn(Mono.just(cliente));

        StepVerifier.create(crearClienteUseCase.crearCliente(cliente))
                .expectNext(cliente)
                .verifyComplete();
    }

    private Cliente.ClienteBuilder getCliente() {
        return Cliente.builder()
                .idCliente("idCliente")
                .cedula("cedula")
                .nombre("nombre")
                .direccion("direccion")
                .telefono("telefono")
                .ruta(1)
                .calificaciones(Collections.singletonList(getCalificacion()
                        .build()));
    }

    private Calificacion.CalificacionBuilder getCalificacion() {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.BUENO);
    }
}