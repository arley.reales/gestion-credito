package co.com.reales.model.common.ex;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ClienteException extends RuntimeException {
    @Getter
    @AllArgsConstructor
    public enum Type {
        EXISTE_CLIENTE( "El cliente ya existe", "CE401");

        private final String messageFrontEnd;
        private final String codigoError;

        public ClienteException build() {
            return new ClienteException(this);
        }
    }

    private final ClienteException.Type type;

    private ClienteException(ClienteException.Type type) {
        super(type.messageFrontEnd);
        this.type = type;
    }

    public ClienteException.Type getType() {
        return type;
    }
}
