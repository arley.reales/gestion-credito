package co.com.reales.model.calificacion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class Calificacion {
    private final TipoCalificacion tipoCalificacion;
}
