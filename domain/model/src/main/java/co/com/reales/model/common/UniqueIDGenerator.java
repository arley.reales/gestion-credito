package co.com.reales.model.common;

import java.util.Date;
import java.util.UUID;

public class UniqueIDGenerator {
    private UniqueIDGenerator() {
    }

    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static Date now() {
        return new Date();
    }
}
