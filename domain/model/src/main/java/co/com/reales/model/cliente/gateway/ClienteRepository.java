package co.com.reales.model.cliente.gateway;

import co.com.reales.model.cliente.Cliente;
import reactor.core.publisher.Mono;

public interface ClienteRepository {
    Mono<Cliente> crearCliente(Cliente cliente);
    Mono<Boolean> existeCliente(String cedulaCliente);
}
