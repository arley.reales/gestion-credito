package co.com.reales.model.cliente;

import co.com.reales.model.calificacion.Calificacion;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class Cliente {
    private final String idCliente;
    private final String idCobro;
    private final String cedula;
    private final String nombre;
    private final String direccion;
    private final String telefono;
    private final Integer ruta;
    private final List<Calificacion> calificaciones;
}
