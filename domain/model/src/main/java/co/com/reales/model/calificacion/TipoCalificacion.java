package co.com.reales.model.calificacion;

public enum TipoCalificacion {
    BUENO("BUENO"),
    REGULAR("REGULAR"),
    MALO("MALO"),
    CLAVO("CLAVO");

    private final String calificacion;

    TipoCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    @Override
    public String toString() {
        return calificacion;
    }
}
