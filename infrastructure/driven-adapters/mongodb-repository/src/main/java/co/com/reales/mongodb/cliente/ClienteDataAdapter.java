package co.com.reales.mongodb.cliente;

import co.com.reales.model.cliente.Cliente;
import co.com.reales.model.cliente.gateway.ClienteRepository;
import co.com.reales.mongodb.cliente.data.ClienteData;
import co.com.reales.reactive.repository.mongo.AdapterOperations;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import static co.com.reales.mongodb.cliente.ClienteDataConvertidor.convertirDominioAData;

@Repository
public class ClienteDataAdapter extends AdapterOperations<Cliente, ClienteData, String, ClienteDataRepository> implements ClienteRepository {

    @Autowired
    public ClienteDataAdapter(ClienteDataRepository repository, ObjectMapper mapper) {
        super(repository, mapper, ClienteDataConvertidor::convertirDataADominio);
    }

    @Override
    public Mono<Cliente> crearCliente(Cliente cliente) {
        return doQuery(repository.save(convertirDominioAData(cliente)));
    }

    @Override
    public Mono<Boolean> existeCliente(String cedulaCliente) {
        return repository.existsByCedula(cedulaCliente);
    }
}
