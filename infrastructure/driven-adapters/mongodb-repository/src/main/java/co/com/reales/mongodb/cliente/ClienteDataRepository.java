package co.com.reales.mongodb.cliente;

import co.com.reales.mongodb.cliente.data.ClienteData;
import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface ClienteDataRepository extends ReactiveCrudRepository<ClienteData, String>, ReactiveQueryByExampleExecutor<ClienteData> {
    Mono<Boolean> existsByCedula(String cedula);
}
