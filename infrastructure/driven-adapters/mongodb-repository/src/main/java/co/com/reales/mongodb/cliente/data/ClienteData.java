package co.com.reales.mongodb.cliente.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@Document("clientes")
public class ClienteData {
    @Id
    private String idCliente;
    @Indexed(name = "index_idCobro")
    private String idCobro;
    private String cedula;
    private String nombre;
    private String direccion;
    private String telefono;
    private Integer ruta;
    private List<CalificacionData> calificaciones;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClienteData)) return false;
        ClienteData that = (ClienteData) o;
        return Objects.equals(idCliente, that.idCliente)
                && Objects.equals(idCobro, that.idCobro)
                && Objects.equals(cedula, that.cedula)
                && Objects.equals(nombre, that.nombre)
                && Objects.equals(direccion, that.direccion)
                && Objects.equals(telefono, that.telefono)
                && Objects.equals(ruta, that.ruta)
                && Objects.equals(calificaciones, that.calificaciones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCliente, idCobro, cedula, nombre, direccion, telefono, ruta, calificaciones);
    }
}
