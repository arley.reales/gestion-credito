package co.com.reales.mongodb.cliente;

import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import co.com.reales.mongodb.cliente.data.CalificacionData;
import co.com.reales.mongodb.cliente.data.ClienteData;

import java.util.List;
import java.util.stream.Collectors;

public class ClienteDataConvertidor {
    private ClienteDataConvertidor() {
    }

    public static Cliente convertirDataADominio(ClienteData clienteData) {
        return Cliente.builder()
                .idCliente(clienteData.getIdCliente())
                .idCobro(clienteData.getIdCobro())
                .cedula(clienteData.getCedula())
                .nombre(clienteData.getNombre())
                .direccion(clienteData.getDireccion())
                .telefono(clienteData.getTelefono())
                .ruta(clienteData.getRuta())
                .calificaciones(convetirListaDataADominio(clienteData.getCalificaciones()))
                .build();
    }

    private static List<Calificacion> convetirListaDataADominio(List<CalificacionData> calificaciones) {
        return calificaciones.stream()
                .map(ClienteDataConvertidor::convertirDataADominio)
                .collect(Collectors.toList());
    }

    private static Calificacion convertirDataADominio(CalificacionData calificacionData) {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.valueOf(calificacionData.getTipoCalificacion()))
                .build();
    }

    public static ClienteData convertirDominioAData(Cliente cliente) {
        ClienteData clienteData = new ClienteData();
        clienteData.setIdCliente(cliente.getIdCliente());
        clienteData.setIdCobro(cliente.getIdCobro());
        clienteData.setCedula(cliente.getCedula());
        clienteData.setNombre(cliente.getNombre());
        clienteData.setDireccion(cliente.getDireccion());
        clienteData.setTelefono(cliente.getTelefono());
        clienteData.setRuta(cliente.getRuta());
        clienteData.setCalificaciones(convetirListaDominioAData(cliente.getCalificaciones()));
        return clienteData;
    }

    private static List<CalificacionData> convetirListaDominioAData(List<Calificacion> calificaciones) {
        return calificaciones.stream()
                .map(ClienteDataConvertidor::convertirDominioAData)
                .collect(Collectors.toList());
    }

    private static CalificacionData convertirDominioAData(Calificacion calificacion) {
        CalificacionData calificacionData = new CalificacionData();
        calificacionData.setTipoCalificacion(calificacion.getTipoCalificacion().toString());
        return calificacionData;
    }
}
