package co.com.reales.mongodb.cliente.data;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CalificacionData {
    private String tipoCalificacion;
}
