package co.com.reales.mongodb.cliente;

import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import co.com.reales.mongodb.cliente.data.CalificacionData;
import co.com.reales.mongodb.cliente.data.ClienteData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClienteDataAdapterTest {
    @Mock
    private ClienteDataRepository clienteDataRepository;
    @Mock
    private ObjectMapper mapper;
    @Mock
    private ReactiveMongoTemplate reactiveMongoTemplate;

    @InjectMocks
    private ClienteDataAdapter clienteDataAdapter;

    private final static String ID_COBRO = "idCobro";

    @Test
    public void testCrearCliente(){
        Cliente cliente = getCliente()
                .build();

        when(clienteDataRepository.save(any(ClienteData.class))).thenReturn(Mono.just(getClienteData()));

        StepVerifier.create(clienteDataAdapter.crearCliente(getCliente().build()))
                .expectNext(cliente)
                .verifyComplete();
    }

    @Test
    public void testExisteCliente(){
        Cliente cliente = getCliente()
                .build();

        when(clienteDataRepository.existsByCedula(cliente.getCedula()))
                .thenReturn(Mono.just(Boolean.TRUE));

        StepVerifier.create(clienteDataAdapter.existeCliente(cliente.getCedula()))
                .expectNext(Boolean.TRUE)
                .verifyComplete();
    }

    private Cliente.ClienteBuilder getCliente() {
        return Cliente.builder()
                .idCliente("idCliente")
                .idCobro(ID_COBRO)
                .cedula("cedula")
                .nombre("nombre")
                .direccion("direccion")
                .telefono("telefono")
                .ruta(1)
                .calificaciones(Collections.singletonList(getCalificacion()
                        .build()));
    }

    private Calificacion.CalificacionBuilder getCalificacion() {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.BUENO);
    }

    private ClienteData getClienteData() {
        ClienteData clienteData = new ClienteData();
        clienteData.setIdCliente("idCliente");
        clienteData.setIdCobro(ID_COBRO);
        clienteData.setCedula("cedula");
        clienteData.setNombre("nombre");
        clienteData.setDireccion("direccion");
        clienteData.setTelefono("telefono");
        clienteData.setRuta(1);
        clienteData.setCalificaciones(Collections.singletonList(getCalificacionData()));
        return clienteData;
    }

    private CalificacionData getCalificacionData() {
        CalificacionData calificacionData = new CalificacionData();
        calificacionData.setTipoCalificacion(TipoCalificacion.BUENO.toString());
        return calificacionData;
    }
}