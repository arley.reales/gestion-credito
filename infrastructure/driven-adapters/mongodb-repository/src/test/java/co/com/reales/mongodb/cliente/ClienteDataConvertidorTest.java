package co.com.reales.mongodb.cliente;

import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import co.com.reales.mongodb.cliente.data.CalificacionData;
import co.com.reales.mongodb.cliente.data.ClienteData;
import org.junit.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class ClienteDataConvertidorTest {

    private final static String ID_COBRO = "idCobro";

    @Test
    public void convertirDataADominio() {
        Cliente cliente = getCliente().build();

        Cliente clienteConvertido = ClienteDataConvertidor.convertirDataADominio(getClienteData());

        assertThat(cliente).isEqualTo(clienteConvertido);
    }

    @Test
    public void convertirDominioAData() {
        ClienteData clienteData = getClienteData();

        ClienteData clienteDataConvertido = ClienteDataConvertidor.convertirDominioAData(getCliente().build());

        assertThat(clienteData).isEqualTo(clienteDataConvertido);
    }

    private Cliente.ClienteBuilder getCliente() {
        return Cliente.builder()
                .idCliente("idCliente")
                .idCobro(ID_COBRO)
                .cedula("cedula")
                .nombre("nombre")
                .direccion("direccion")
                .telefono("telefono")
                .ruta(1)
                .calificaciones(Collections.singletonList(getCalificacion()
                        .build()));
    }

    private Calificacion.CalificacionBuilder getCalificacion() {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.BUENO);
    }

    private ClienteData getClienteData() {
        ClienteData clienteData = new ClienteData();
        clienteData.setIdCliente("idCliente");
        clienteData.setIdCobro(ID_COBRO);
        clienteData.setCedula("cedula");
        clienteData.setNombre("nombre");
        clienteData.setDireccion("direccion");
        clienteData.setTelefono("telefono");
        clienteData.setRuta(1);
        clienteData.setCalificaciones(Collections.singletonList(getCalificacionData()));
        return clienteData;
    }

    private CalificacionData getCalificacionData() {
        CalificacionData calificacionData = new CalificacionData();
        calificacionData.setTipoCalificacion(TipoCalificacion.BUENO.toString());
        return calificacionData;
    }
}