package co.com.reales.controladores;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

public abstract class ControladorBase {
    protected <T> Mono<ResponseEntity<T>> validadEntidad(Mono<T> valor) {

        return valor.map(entidad ->
                ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(entidad))
                .switchIfEmpty(Mono.just(ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.APPLICATION_JSON).build()));

    }
}
