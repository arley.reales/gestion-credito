package co.com.reales.controladores.cliente.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class CalificacionDTO {
    private final String tipoCalificacion;
}
