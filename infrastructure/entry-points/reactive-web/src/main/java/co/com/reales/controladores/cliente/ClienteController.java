package co.com.reales.controladores.cliente;

import co.com.reales.controladores.ControladorBase;
import co.com.reales.controladores.cliente.dto.ClienteDTO;
import co.com.reales.model.common.UniqueIDGenerator;
import co.com.reales.usecase.cliente.CrearClienteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api/v1/gestion-credito", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class ClienteController extends ControladorBase {
    private final CrearClienteUseCase crearClienteUseCase;

    @PostMapping(value = "/clientes")
    public Mono<ResponseEntity<ClienteDTO>> crearCliente(@RequestBody ClienteDTO clienteDTO) {
        return validadEntidad(
                crearClienteUseCase.crearCliente(ClienteDTO.convertirDtoADominioInsercion(clienteDTO, UniqueIDGenerator.uuid()))
                        .map(cliente -> ClienteDTO.convertirDominioADto(cliente))
        );
    }
}
