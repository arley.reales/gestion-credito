package co.com.reales.controladores.healthy;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "api/v1/gestion-credito")
public class ChequeoEstadoController {
    @GetMapping(value = "/healthy")
    public Mono<String> getHealthy() {
        return Mono.just("ok");
    }
}
