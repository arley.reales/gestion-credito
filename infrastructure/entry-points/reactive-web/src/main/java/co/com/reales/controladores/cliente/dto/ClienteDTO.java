package co.com.reales.controladores.cliente.dto;

import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class ClienteDTO {
    private final String idCliente;
    private final String idCobro;
    private final String cedula;
    private final String nombre;
    private final String direccion;
    private final String telefono;
    private final Integer ruta;
    private final List<CalificacionDTO> calificaciones;

    public static Cliente convertirDtoADominioInsercion(ClienteDTO clienteDTO, String idCliente) {
        return Cliente.builder()
                .idCliente(idCliente)
                .idCobro(clienteDTO.getIdCobro())
                .cedula(clienteDTO.getCedula())
                .nombre(clienteDTO.getNombre())
                .direccion(clienteDTO.getDireccion())
                .telefono(clienteDTO.getTelefono())
                .ruta(clienteDTO.getRuta())
                .calificaciones(convetirListaDtoADominio(clienteDTO.getCalificaciones()))
                .build();
    }

    private static List<Calificacion> convetirListaDtoADominio(List<CalificacionDTO> calificaciones) {
        return calificaciones.stream()
                .map(calificacionDTO -> convertirCalificacionDtoADominio(calificacionDTO))
                .collect(Collectors.toList());
    }

    private static Calificacion convertirCalificacionDtoADominio(CalificacionDTO calificacionDTO) {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.valueOf(calificacionDTO.getTipoCalificacion()))
                .build();
    }

    public static ClienteDTO convertirDominioADto(Cliente cliente) {
        return ClienteDTO.builder()
                .idCliente(cliente.getIdCliente())
                .idCobro(cliente.getIdCobro())
                .cedula(cliente.getCedula())
                .nombre(cliente.getNombre())
                .direccion(cliente.getDireccion())
                .telefono(cliente.getTelefono())
                .ruta(cliente.getRuta())
                .calificaciones(convetirListaDominioADto(cliente.getCalificaciones()))
                .build();
    }

    private static List<CalificacionDTO> convetirListaDominioADto(List<Calificacion> calificaciones) {
        return calificaciones.stream()
                .map(calificacion -> convertirCalificacionDominioADto(calificacion))
                .collect(Collectors.toList());
    }

    private static CalificacionDTO convertirCalificacionDominioADto(Calificacion calificacion) {
        return CalificacionDTO.builder()
                .tipoCalificacion(calificacion.getTipoCalificacion().toString())
                .build();
    }
}
