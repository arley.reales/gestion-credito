package co.com.reales.config;

import co.com.reales.model.cliente.Cliente;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
public class ObjectMapperConfigTest {

    private final Cliente cliente = Cliente.builder().cedula("cedula").build();

    @Test
    public void validarNoRetorneNull() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapperConfig().mapper();
        log.info(objectMapper.writeValueAsString(cliente));
        assertThat(objectMapper.writeValueAsString(cliente).contains("idCobro")).isFalse();
    }
}