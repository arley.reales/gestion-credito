package co.com.reales.controladores.healthy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebFluxTest(ChequeoEstadoController.class)
public class ChequeoEstadoControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    private static final String APIENDPOINT = "/healthy";
    private static final String CONTENTTYPE = "application/json";

    @Test
    public void testChequeoApp() {

        final WebTestClient.ResponseSpec spec = webTestClient.get().uri(uriBuilder -> uriBuilder.path("/api/v1/gestion-credito" + APIENDPOINT)
                .build())
                .header("Content-Type", CONTENTTYPE)
                .exchange();

        spec.expectBodyList(String.class).consumeWith(res -> {
            HttpStatus status = res.getStatus();
            assertThat(status.is2xxSuccessful()).isTrue();
        });
    }

}