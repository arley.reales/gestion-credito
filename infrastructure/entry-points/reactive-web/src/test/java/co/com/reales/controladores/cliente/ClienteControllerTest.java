package co.com.reales.controladores.cliente;

import co.com.reales.controladores.cliente.dto.CalificacionDTO;
import co.com.reales.controladores.cliente.dto.ClienteDTO;
import co.com.reales.model.calificacion.Calificacion;
import co.com.reales.model.calificacion.TipoCalificacion;
import co.com.reales.model.cliente.Cliente;
import co.com.reales.usecase.cliente.CrearClienteUseCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static reactor.core.publisher.Mono.empty;
import static reactor.core.publisher.Mono.just;

@RunWith(SpringRunner.class)
@WebFluxTest(ClienteController.class)
public class ClienteControllerTest {
    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private CrearClienteUseCase crearClienteUseCase;

    private static final String APIENDPOINT = "/clientes";
    private static final String CONTENTTYPE = "application/json";

    @Test
    public void testCrearClienteRespuesta200() {

        when(crearClienteUseCase.crearCliente(any()))
                .thenReturn(just(getCliente().build()));

        final WebTestClient.ResponseSpec spec = webTestClient.post().uri(uriBuilder -> uriBuilder.path("/api/v1/gestion-credito" + APIENDPOINT).build())
                .contentType(MediaType.APPLICATION_JSON)
                .header("Content-Type", CONTENTTYPE)
                .accept(MediaType.APPLICATION_JSON)
                .body(just(getClienteDto().build()), ClienteDTO.class)
                .exchange();

        spec.expectBody(ClienteDTO.class).consumeWith(res -> {
            HttpStatus status = res.getStatus();
            assertThat(status.is2xxSuccessful()).isTrue();
        });
    }

    @Test
    public void testCrearClienteRespuesta400() {

        when(crearClienteUseCase.crearCliente(any()))
                .thenReturn(empty());

        final WebTestClient.ResponseSpec spec = webTestClient.post().uri(uriBuilder -> uriBuilder.path("/api/v1/gestion-credito" + APIENDPOINT).build())
                .contentType(MediaType.APPLICATION_JSON)
                .header("Content-Type", CONTENTTYPE)
                .accept(MediaType.APPLICATION_JSON)
                .body(just(getClienteDto().build()), ClienteDTO.class)
                .exchange();

        spec.expectBody(ClienteDTO.class).consumeWith(res -> {
            HttpStatus status = res.getStatus();
            assertThat(status.is4xxClientError()).isTrue();
        });
    }

    private Cliente.ClienteBuilder getCliente() {
        return Cliente.builder()
                .idCliente("idCliente")
                .cedula("cedula")
                .nombre("nombre")
                .direccion("direccion")
                .telefono("telefono")
                .ruta(1)
                .calificaciones(Collections.singletonList(getCalificacion()
                        .build()));
    }

    private Calificacion.CalificacionBuilder getCalificacion() {
        return Calificacion.builder()
                .tipoCalificacion(TipoCalificacion.BUENO);
    }

    private ClienteDTO.ClienteDTOBuilder getClienteDto() {
        return ClienteDTO.builder()
                .idCliente("idCliente")
                .cedula("cedula")
                .nombre("nombre")
                .direccion("direccion")
                .telefono("telefono")
                .ruta(1)
                .calificaciones(Collections.singletonList(getCalificacionDto()
                        .build()));
    }

    private CalificacionDTO.CalificacionDTOBuilder getCalificacionDto() {
        return CalificacionDTO.builder()
                .tipoCalificacion(TipoCalificacion.BUENO.toString());
    }
}