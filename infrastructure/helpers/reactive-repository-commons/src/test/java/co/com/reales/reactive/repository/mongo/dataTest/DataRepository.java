package co.com.reales.reactive.repository.mongo.dataTest;

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

interface DataRepository extends ReactiveCrudRepository<DataEntity, String>, ReactiveQueryByExampleExecutor<DataEntity> {
}

