package co.com.reales.reactive.objects.utils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class ListValidators {

    private ListValidators() {
    }

    public static <T, C> List<T> validarLista(List<C> lista, Function<List<C>, List<T>> funcionAEjecutar) {
        return Optional.ofNullable(lista)
                .map(cs -> funcionAEjecutar.apply(lista))
                .orElse(Collections.emptyList());
    }

    public static <T, K> boolean validarRepetidos(List<T> list, Function<T, K> funcionAEjecutar) {

        if (validarListaVaciaONula(list)) return false;

        return list.stream()
                .collect(Collectors.groupingBy(funcionAEjecutar))
                .size() != list.size();
    }

    public static <T> boolean validarListaVaciaONula(List<T> lista) {
        return Objects.isNull(lista) || lista.isEmpty();
    }

    public static <T, K> List<K> validarListaVaciaRetorneVacio(List<T> list, Function<T, K> function) {

        if (validarListaVaciaONula(list)) return Collections.emptyList();

        return list.stream()
                .map(function)
                .collect(Collectors.toList());
    }

    public static <T> Predicate<T> distinctBy(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
