package co.com.reales.reactive.objects.utils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ObjectValidators {

    private ObjectValidators() {
    }

    public static String validacionTextoNulo(String texto) {
        return ObjectValidators.campoVacio(texto) ? null : texto;
    }

    public static Optional<String> validarStringOptional(String mensajeValidar) {
        if (campoVacio(mensajeValidar)){
            return Optional.empty();
        }
        return Optional.of(mensajeValidar);
    }
    public static String obtenerFormatoPeriodoCapacidad(LocalDate localDate) {
        return String.valueOf(localDate.getYear()).concat(String.format("%02d", localDate.getMonthValue()));
    }
    public static Optional<LocalDate> validarLocalDateOptional(LocalDate localDateValidar) {
        return Objects.isNull(localDateValidar)
                ? Optional.empty()
                : Optional.of(localDateValidar);
    }
    public static BigDecimal validarIntegerOptional(BigDecimal numeroValidar) {
        if (numeroValidar == null){
            return new BigDecimal(0);
        }
        return numeroValidar;
    }

    public static boolean campoVacio(String str) {
        return str == null || str.trim().isEmpty();
    }

    private static boolean numeroValido(Integer integer) {
        return integer < 0;
    }


    public static boolean validarCamposNulos(Object... objects) {

        return !Arrays.stream(objects)
                .filter(ObjectValidators::esNuloOVacio)
                .collect(Collectors.toList()).isEmpty();
    }

    public static boolean esNuloOVacio(Object object) {

        if (Objects.isNull(object)) return true;

        switch (object.getClass().getSimpleName()) {

            case "String":
                return campoVacio(String.valueOf(object));

            case "Integer":
                return numeroValido((Integer) object);

            default:
                return false;
        }
    }

    public static LocalDate convertirDateALocalDate(Date date) {
        return date  == null ? null :date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static Optional<LocalTime> convertirStringALocalTime(String date) {
        return date  == null ? Optional.empty() : Optional.of(LocalTime.parse(date));
    }

    public static LocalDate convertirDateALocalDatea(LocalDate localDate) {
        return localDate;
    }



    public static Optional<LocalDate> convertirDateAOptionalLocalDate(Date date) {
        return date  == null ? Optional.empty() : Optional.of(date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate());
    }

    public static Date convertirLocalDateADate(LocalDate localDate) {
        return localDate == null ? null :java.util.Date.from(localDate.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static Optional<Date> convertirOptionalMillisAOptionalDate(Optional<String> fechaMillis) {
        return fechaMillis.map(fecha -> Date.from(Instant.ofEpochMilli(Long.parseLong(fecha))));
    }

    public static <T> Boolean biOptionalPresent(Optional<T> optionalT1, Optional<T> optionalT2) {
        return (optionalT1.isPresent() && optionalT2.isPresent());
    }
    public static LocalDate convertirStringALocalDate(String fecha) {
        return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    public static LocalDate convertirMillisALocalDate(String fechaString) {
        return convertirDateALocalDate(Date.from(Instant.ofEpochMilli(Long.parseLong(fechaString))));
    }
}
